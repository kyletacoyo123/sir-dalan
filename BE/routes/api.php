<?php
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::resource('Categories', 'CategoryController');
Route::resource('Books', "Book_controller");
Route::resource('Patrons', "Patron_controller");
Route::resource('Borrowed_Book', 'BorrowedBook_controller');
Route::resource('Returned_Books', 'ReturnedBook_controller');
