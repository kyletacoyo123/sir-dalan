<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class borrowedBooks extends Model
{
    use HasFactory;
    protected $table = 'borrowed_books';
    protected  $fillable = ['patron_id', 'copies', 'book_id'];

    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }
    public function patron()
    {
        return $this->belongsTo(patrons::class, 'patron_id');
    }
}
