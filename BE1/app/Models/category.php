<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    

    
    
    protected $table = 'categories';
    protected $fillable = ['category'];

    public function books()
    {
        return $this->hasMany(Book::class, 'category_id');
    }

}
