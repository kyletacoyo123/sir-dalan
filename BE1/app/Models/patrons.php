<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class patrons extends Model
{
    use HasFactory;
    protected  $fillable = ['Last_name', 'First_name', 'Middle_name', 'Email'];


    public function returned()
    {
        return $this->hasMany(returnedBooks ::class, 'patron_id');
    }
    
    public function borrowed()
    {
        return $this->hasMany(borrowedBooks::class, 'patron_id');
    }
}
