<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Book extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function category()
    {
        return $this->belongsTo(category::class, 'category_id');
    }

    public function borrowed()
    {
        return $this->hasMany(borrowedBooks::class, 'book_id');
    }
    public function returned()
    {
        return $this->hasMany(returnedBooks::class, 'book_id');
    }
}
