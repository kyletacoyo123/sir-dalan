<?php

namespace App\Http\Controllers;

use App\Models\patrons;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patrons = patrons::all();
        return response()->json([
            "message" => "Patron List",
            "data" => $patrons]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $patrons = new patrons();

        $patrons->Last_name = $request->Last_name;
        $patrons->First_name = $request->First_name;
        $patrons->Middle_name = $request->Middle_name;
        $patrons->Email = $request->Email;

        $patrons->save();
        return response()->json($patrons);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patrons = patrons::find($id);
        return response()->json($patrons);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patrons = patrons::find($id);
        
        $patrons->Last_name = $request->input('Last_name');
        $patrons->first_name = $request->input('First_name');
        $patrons->Middle_name = $request->input('Middle_name');
        $patrons->Email = $request->input('Email');

        $patrons->update();
        return response()->json($patrons);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patrons = patrons::find($id);
        $patrons->delete();
        return response()->json($patrons);
    }
}
