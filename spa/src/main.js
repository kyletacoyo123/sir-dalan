import Vue from 'vue';
import crud from "../src/crud.vue";
window.axios = require('axios');
Vue.config.productionTip = false;
new Vue({
  render: h => h(crud),}).$mount('#app');

